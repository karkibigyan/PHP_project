<?php 
    session_start();
    require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
    require INC_PATH.'header.php';
    require INC_PATH.'functions.php';
?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <?php flash(); ?>
                	<form method="post" name="login" action="login.php">
						<div class="form-group">
							<label>Username</label>
							<input type="text" name="username" class="form-control" id="username" required />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control" id="password" required />
						</div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-primary" id="submit" required />
						</div>
                	</form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php require INC_PATH.'footer.php';?>