<?php 
    session_start();
    require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
    require INC_PATH.'db.php';
    require INC_PATH.'header.php';

    require INC_PATH.'functions.php';
    require INC_PATH.'navigation.php';
?>
        <div id="page-wrapper">

            <div class="container-fluid">
                
                <?php flash();?>
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            User List
                            <a href="user-add.php" class="btn btn-success pull-right">
                            	<i class="fa fa-plus"></i> Add User
                            </a>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="dashboard.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-users"></i> User List
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered table-responsive">
							<thead>
								<th>S.N.</th>
								<th>Name</th>
                                <th>Email</th>
                                <th>Type</th>
								<th>Status</th>
								<th>Action</th>
							</thead>
                            <tbody>
                                <?php 
                                   $user_info = getAllUser();
                                   if($user_info){
                                       foreach($user_info as $key=>$users){
                                        ?>
                                        <tr>
                                            <td><?php echo $key+1;?></td>
                                            <td><?php echo $users['full_name'];?></td>
                                            <td><?php echo $users['email'];?></td>
                                            <td><?php echo getUserType($users['role_id']);?></td>
                                            <td><?php echo getStatusType($users['status']);?></td>
                                            <td>
                                                <a href="" class="btn btn-success" style="border-radius: 50%">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <?php 
                                                    $url = "user-process.php?id=".$users['id']."&act=".substr(md5('delete-'.$users['id']), 0, 15);
                                                ?>
                                                <a href="<?php echo $url;?>" onclick="return confirm('Are you sure you want to delete this user? The user will not be recovered back after deletion.')" class="btn btn-danger" style="border-radius: 50%">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a href="" class="btn btn-warning" style="border-radius: 50%">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                       }
                                   } else {
                                        echo '<tr><td colspan="5">Sorry! There are no any users in the database</td></tr>';
                                   }
                                ?>
                            </tbody>
						</table>
					</div>
				</div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include 'inc/footer.php';?>