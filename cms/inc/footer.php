<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-13 16:26:45
 * @Organization: Knockout System Pvt. Ltd.
 */
?>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo JS_URL;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo JS_URL;?>bootstrap.min.js"></script>
</body>

</html>
