<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-13 16:30:18
 * @Organization: Knockout System Pvt. Ltd.
 */

const SITE_URL = "http://localhost/blog/";

const CMS_URL = SITE_URL."cms/";

const ASSETS_URL = CMS_URL.'assets/';
const JS_URL = ASSETS_URL.'js/';
const CSS_URL = ASSETS_URL.'css/';
const FONT_AWESOME_URL = ASSETS_URL.'font-awesome/';

define('INC_PATH',$_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/');


const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = '';
const DB_DATABASE = 'blog';
// const DB_SOCKET = 3306;

