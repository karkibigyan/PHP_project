<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-15 15:27:52
 * @Organization: Knockout System Pvt. Ltd.
 */


function debugger($data, $is_die = false){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	if($is_die){
		exit;
	}
}

function getAllUser(){
	global $conn;
	$sql = "SELECT * FROM users ORDER BY id DESC";
	$query = mysqli_query($conn, $sql);
	if($query){
		if(mysqli_num_rows($query) > 0){
			$data = array();
			while($row = mysqli_fetch_assoc($query)){
				$data[] = $row;
			}
			return $data;
		} else {
			return false;
		}
	} else {
		return false;
	}
}


function sanitize($string){
	global $conn;
	return mysqli_real_escape_string($conn, $string);
}



function getRandomString($length = 15){
	$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$len = strlen($chars);
	$random = '';

	for($i=0; $i<$length; $i++){
		$random .= $chars[rand(0,$len-1)];
	}
	return $random;
}

function addUser($post){
	global $conn;
	$sql = "INSERT INTO users SET 
			full_name = '".$post['full_name']."',
			email = '".$post['email']."',
			password = '".$post['password']."',
			role_id = ".$post['role_id'].",
			status = ".$post['status'].",
			token = '".$post['token']."'
			 ";

	$query = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	//debugger($conn, true);

	if($query){
		$success = $conn->insert_id;
	} else {
		$success = false;
	}
	return $success;
}

function getUserByUsername($username){
	global $conn;
	$sql = "SELECT * FROM users WHERE email = '".$username."'";
	$query = mysqli_query($conn, $sql);

	if($query){
		if(mysqli_num_rows($query) <= 0){	// $query->num_rows 
			return false;
		} else {
			$data = mysqli_fetch_assoc($query);
			return $data;
		}
	} else {
		return false;
	}
}

function flash(){
	include INC_PATH.'notification.php';
}

function getUserType($role_id){
	if($role_id == 1){
		return "Admin";
	} else if($role_id == 2){
		return "Editor";
	} else if($role_id == 3){
		return "Reporter";
	} else {
		return "User";
	}
}

function getStatusType($status){
	if($status == 1){
		return "Active";
	} else if($status == 2){
		return "Inactive";
	} else if($status == 3){
		return "Suspended";
	} else {
		return "Undefined";
	}	
}

function getUserbyId($id){
	global $conn;
	$sql = "SELECT * FROM users WHERE id = ".$id;
	$query = mysqli_query($conn, $sql);
	if(mysqli_num_rows($query) > 0){
		return mysqli_fetch_assoc($query);
	} else {
		return false;
	}
}

function deleteData($table, $field, $data){
	global $conn;
	$sql = "DELETE FROM ".$table." WHERE ".$field." = '".$data."'";
	
	$query = mysqli_query($conn, $sql);
	if($query){
		return true;
	} else {
		return false;
	}
}