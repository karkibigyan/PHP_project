<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-15 15:27:01
 * @Organization: Knockout System Pvt. Ltd.
 */
ob_start();
session_start();

require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
require INC_PATH.'db.php';

require INC_PATH."functions.php";

if(isset($_POST) && !empty($_POST)){
	$post = $_POST;
	$post['password'] = sha1($post['email'].$post['password']);
	$post['token'] = getRandomString();
	//debugger($post, true);

	$success = addUser($post);
	if($success){
		$_SESSION['success'] = "User has been registered successfully. The user id is: ".$success;
		@header('location: user.php');
		exit;
	} else {
		$_SESSION['error'] = "Sorry! There was problem while registering user.";
		@header('location: user.php');
		exit;
	}
} else if(isset($_GET['id'], $_GET['act']) && $_GET['id'] != ""){
	$id = (int)sanitize($_GET['id']);
	
	if($_GET['act'] === substr(md5('delete-'.$id), 0, 15)){
		
		$user_info = getUserbyId($id);
		
		if($user_info){
			$del = deleteData('users', 'id', $id);
		
			if($del){
				$_SESSION['success'] = "User deleted successfully.";
				header('location: user.php');
				exit;

			} else {
				$_SESSION['error'] = "Sorry! There was problem while deleting user information.";
				header('location: user.php');
				exit;

			}
		} else {
			$_SESSION['warning'] = "User already deleted or does not exists.";
			header('location: user.php');
			exit;

		}
	} else {
		$_SESSION['error'] = "Undefined action.";
		header('location: user.php');
		exit;

	}
}else {
	$_SESSION['error'] = "Unauthorized access";
	header('location: user.php');
	exit;
}
ob_flush();