<?php 
    require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
    require INC_PATH.'header.php';
    require INC_PATH.'navigation.php';
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            User Add
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="dashboard.php">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-users"></i>  <a href="user.php">List User</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Admin Panel
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				
				<div class="row">
					<div class="col-md-12">
						<form action="user-process.php" method="post" class="form form-horizontal">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Full Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="full_name" required placeholder="Full Name" class="form-control" id="name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Email:</label>
                                <div class="col-sm-8">
                                    <input type="email" name="email" required placeholder="Your email address" class="form-control" id="email">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Password:</label>
                                <div class="col-sm-8">
                                    <input type="password" name="password" required class="form-control" id="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">User Type:</label>
                                <div class="col-sm-8">
                                    <select name="role_id" id="role_id" required class="form-control">
                                        <option value="1">Admin</option>
                                        <option value="2">Editor</option>
                                        <option value="3">Reporter</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Status:</label>
                                <div class="col-sm-8">
                                    <select name="status" id="status" required class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                        <option value="2">Suspended</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label"></label>
                                <div class="col-sm-8">
                                    <button class="btn btn-success" id="submit">
                                        <i class="fa fa-send"></i> Submit
                                    </button>
                                </div>
                            </div>

                        </form>
					</div>
				</div>
            </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include 'inc/footer.php';?>