<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-22 15:46:42
 * @Organization: Knockout System Pvt. Ltd.
 */
	ob_start();
	session_start();
	require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
	require INC_PATH.'db.php';
	require INC_PATH.'functions.php';

	if(isset($_POST) && !empty($_POST)){
		$username = sanitize($_POST['username']);
		$password = sha1($username.$_POST['password']);
		//echo $password;
		$user_info = getUserByUsername($username);
		//debugger($user_info, true);

		if($user_info){
			if($password === $user_info['password']){
				if($user_info['status'] == 1){
					$_SESSION['success'] = "Welcome to admin panel! ".$user_info['full_name'];
					
					$_SESSION['user_id'] = $user_info['id'];
					$_SESSION['role_id'] = $user_info['role_id'];
					$_SESSION['email'] = $user_info['eamil'];
					$_SESSION['full_name'] = $user_info['full_name'];

					@header('location: dashboard.php');
					exit;
				} else {
					$_SESSION['error'] = "User not activated or suspended. Please contact our administrator.";
					@header('location: ./');
					exit;	
				}
			} else {
				$_SESSION['error'] = "Password does not match.";
				@header('location: ./');
				exit;
			}
		} else {
			$_SESSION['error'] = "User not found.";
			@header('location: ./');
			exit;
		}
	} else {
		@header('location: ./');
		exit;
	}
	ob_flush();