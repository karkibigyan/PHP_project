<?php 
    session_start();
    if(!isset($_SESSION['user_id'], $_SESSION['role_id']) || empty($_SESSION['user_id']) || empty($_SESSION['role_id'])){
        $_SESSION['error'] = "Please login first.";
        @header('location: ./');
        exit; 
    }
    require $_SERVER['DOCUMENT_ROOT'].'/blog/cms/inc/config.php';
    require INC_PATH.'header.php';
    require INC_PATH.'functions.php';
    require INC_PATH.'navigation.php';
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <?php flash(); ?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard
                            <small>Blog admin page</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="dashboard.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Admin Panel
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

<?php include 'inc/footer.php';?>