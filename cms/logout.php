<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-12-22 16:18:25
 * @Organization: Knockout System Pvt. Ltd.
 */

session_start();
session_destroy();

@header('location: ./');
exit;